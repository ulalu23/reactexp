var app=require('../app');
var debug=require('debug')('mern-app: server');
var http=require('http');

var port= normalizePort(process.env.PORT || '3000');
app.set('port',port);

var server=http.createServer(app);
server.listen(port);
server.on('error',onError);
server.on('listening', onListening);

//funkcje
function normalizePort(val){
    var port=parseInt(val,10);
    if(isNaN(port)){
        return val;
    }
    if(port >= 0){
        return port
    }
    return false;
}
//error event error listenr
function onError(error){
    if (error.syscall !=='listen'){
        throw error;
    }
    var bind= typeof port === 'string'
      ? 'Pipe' + port : 'Port' + port;
//handle specific listen error friendly message
    switch( error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privilages');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + 'is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    var addr=server.address();
    var bind= typeof addr === 'string' ? 'pipe'+ addr : 'port' + addr.port;
    debug('listening on ' + bind )
}


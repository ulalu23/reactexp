var express=require('express');
var path= require('path');
var favicon=require('serve-favicon');
var logger=require('morgan');
var bodyParser= require('body-parser');

var note= require('./routes/note');
var app=express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': 'false'}));
app.use(express.static(path.join(__dirname, 'build')));

app.use('/api/note',note);

app.use(function(req,res,next){
    var err = new Error('not found');
    err.status =404;
    next(err);
});

app.use(function(err,req,res,next){
    res.locals.message =err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.render('error')
});

module.exports = app;